<?php
/**
 * 
 * List of products for comparison
 * url: reviews
 * 
 */
// GET LIST OF FLAGGED NODES

// UNFLAG "FLAGGED" NODES
// $flag->flag('unflag', $nid);

?>


<?php
// WE GET THE FLAGGED NODES
$comparedNodes = compareNodes();

// AND WE LOOP OVER THEM
if(sizeof($comparedNodes) > 0 ){

foreach ($comparedNodes as $singleNode){
// print_r($singleNode);
?>
  
  <div class="nodeToCompare">
  <?php echo '<div class="fieldid fields">' . $singleNode['field_id'] . '</div>'; ?>
  <?php echo '<div class="fieldmanufacturer fields">' . $singleNode['field_product_manufacturer'] . '</div>'; ?>
  <?php echo '<div class="fielddescription fields">' . $singleNode['field_product_description'] . '</div>'; ?>
  <?php echo '<div class="fieldpositive fields">' . $singleNode['field_product_positive'] . '</div>'; ?>
  <?php echo '<div class="fieldnegative fields">' . $singleNode['field_product_negative'] . '</div>'; ?>
  </div>
  
<?php
	}
}

?>

