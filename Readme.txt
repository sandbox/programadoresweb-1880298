

INSTALL
- just uncompress the comparenodes in modules Drupal directory and proceed as usual.
- Once installed, decide which nodes can be compared, activating the compared_node flag in their content product:
  admin/build/flags and then -> admin/build/flags/manage/comparethis_flag
- then, go to your theme and add the "flag this" to your nodes list.
  
- Finally, go to your template and add the flag, simply adding something like this:
  
  <?php $flag = flag_get_flag('comparethis_flag'); ?>
  <?php echo flag_create_link('comparethis_flag', $node->nid); ?>
  
- Once a flag has been flagged to be compared, it will appear in: http://YOUR-DRUPAL-INSTALLATION/comparenodes/
  
- theme your template copying comparenodes.tpl.php in your active theme directory (example> /sites/all/themes/mytheme/)
  
  
  
  